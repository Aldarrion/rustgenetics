use crate::n_queens;

pub trait Fitness {
    fn fitness(a: &Vec<u32>) -> i32;
}

pub struct SimpleFitness {
}

impl Fitness for SimpleFitness {
    fn fitness(a: &Vec<u32>) -> i32 {
        let in_danger = n_queens::in_danger_count(&a);
        a.len() as i32 - in_danger as i32
    }
}