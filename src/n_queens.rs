use std::fmt;

struct Point {
    x: i32,
    y: i32,
}

impl PartialEq for Point {
    fn eq(&self, rhs: &Point) -> bool {
        self.x == rhs.x 
        && self.y == rhs.y
    }
}

impl fmt::Display for Point {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

pub fn in_danger_count(solution: &Vec<u32>) -> u32 {
    let n = solution.len();

    if n == 0 || n == 1 {
        return 0;
    }

    for (x, y) in solution.iter().enumerate() {
        if !is_valid_position(&y, &(n as u32)) {
            println!("queen ({}, {}) is not on valid position", &x, &y);
            return 0;
        }
    }

    let mut in_danger_count = 0;

    for (x1, y1) in solution[..n - 1].iter().enumerate() {
        for (x2, y2) in solution[x1 + 1..].iter().enumerate() {
            let q1 = Point {
                x: x1 as i32,
                y: *y1 as i32,
            };
            let q2 = Point {
                x: (x1 + x2 + 1) as i32,
                y: *y2 as i32,
            };
            //println!("q1 {}, q2 {}", &q1, &q2);
            if is_in_danger(&q1, &q2) {
                //println!("{} endangers {}", &q1, &q2);
                in_danger_count += 1;
            }
        }
    }

    in_danger_count
}

pub fn is_valid_solution(solution: &Vec<u32>) -> bool {
    in_danger_count(solution) == 0
}

fn is_in_danger(q1: &Point, q2: &Point) -> bool {
    q1.y == q2.y
    || q1.y + (q2.x - q1.x) == q2.y
    || q1.y - (q2.x - q1.x) == q2.y
}

fn is_valid_position(y: &u32, n: &u32) -> bool {
    y < n
}