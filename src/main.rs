mod n_queens;
mod genetics;
mod mutation;
mod crossover;
mod fitness;
mod selection;

use std::time::Instant;

//use crate::crossover::Crossover;
//use crate::mutation::Mutation;
//use crate::fitness::Fitness;
//use crate::selection::Selection;

fn main() {
    /*let sol = vec![3, 1, 0, 2];
    let result = n_queens::is_valid_solution(&sol);
    println!("result: {}", result);*/

    /*let a = vec![0, 1, 2, 3, 4, 5];
    let b = vec![5, 4, 3, 2, 1, 0];
    let mut c = crossover::SinglePointCrossover::cross(&a, &b);
    println!("{:?}", c);

    
    mutation.mutate(&mut c);
    println!("{:?}", c);

    println!("In danger: {}", n_queens::in_danger_count(&c));
    println!("Fitness:   {}", fitness::SimpleFitness::fitness(&c));*/

    let mutation = mutation::SimpleMutation::new(0.1);
    let mut sga = genetics::GeneticAlg::<crossover::SinglePointCrossover, mutation::SimpleMutation, selection::TournamentSelection<fitness::SimpleFitness>>::new(mutation);
    let now = Instant::now();
    sga.run(8, 100000);
    let new_now = Instant::now();
    println!("Finished in: {:?}", new_now.duration_since(now));
}