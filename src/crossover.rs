use rand::Rng;

pub trait Crossover {
    fn cross(a: &Vec<u32>, b: &Vec<u32>) -> Vec<u32>;
}

pub struct SinglePointCrossover {
}

impl Crossover for SinglePointCrossover {
    fn cross(a: &Vec<u32>, b: &Vec<u32>) -> Vec<u32> {
        let point = rand::thread_rng().gen_range(0, a.len());
        let mut c = a.clone();
        for (i, b_val) in b[point..].iter().enumerate() {
            c[i + point] = *b_val;
        }
        c
    }
}
