use rand::Rng;
use crate::fitness::Fitness;
use std::marker::PhantomData;

pub trait Selection {
    fn select(population: &Vec<Vec<u32>>) -> &Vec<u32>;
}

pub struct TournamentSelection<TFitness: Fitness> {
    phantom: PhantomData<TFitness>,
}

impl<TFitness: Fitness> Selection for TournamentSelection<TFitness> {
    fn select(population: &Vec<Vec<u32>>) -> &Vec<u32> {
        let n = population.len();
        let a = &population[rand::thread_rng().gen_range(0, n)];
        let b = &population[rand::thread_rng().gen_range(0, n)];
        let a_fitness = TFitness::fitness(&a);
        let b_fitness = TFitness::fitness(&b);
        if a_fitness > b_fitness {
            a
        } else {
            b
        }
    }
}