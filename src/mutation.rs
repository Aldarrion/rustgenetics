use rand::Rng;

pub trait Mutation {
    fn mutate(&self, a: &mut Vec<u32>);
}

pub struct SimpleMutation {
    mut_proba: f32,
}

impl SimpleMutation {
    pub fn new(mut_proba: f32) -> SimpleMutation {
        SimpleMutation {mut_proba}
    }
}

impl Mutation for SimpleMutation {
    fn mutate(&self, a: &mut Vec<u32>) {
        let n = a.len() as u32;
        for i in a {
            let mut_rnd = rand::thread_rng().gen::<f32>();
            if mut_rnd < self.mut_proba {
                if rand::thread_rng().gen::<f32>() < 0.5 {
                    *i = (*i + 1) % n;
                } else {
                    *i = if *i == 0 {n - 1} else {*i - 1}
                }
            }
        }
    }
}