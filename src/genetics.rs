use crate::crossover::Crossover;
use crate::mutation::Mutation;
use crate::selection::Selection;
use crate::n_queens;
use std::marker::PhantomData;
use rand::Rng;
use std::mem;

const POP_SIZE: usize = 200;

pub struct GeneticAlg<TCross: Crossover, TMutation: Mutation, TSelection: Selection> {
    population: Vec<Vec<u32>>,
    next_population: Vec<Vec<u32>>,
    phantom: PhantomData<TCross>,
    sel_phantom: PhantomData<TSelection>,
    mutation: TMutation,
}

impl<TCross: Crossover, TMutation: Mutation, TSelection: Selection> GeneticAlg<TCross, TMutation, TSelection> {
    pub fn new(mutation: TMutation) -> GeneticAlg<TCross, TMutation, TSelection> {
        let population = vec![vec![]; POP_SIZE];
        GeneticAlg::<TCross, TMutation, TSelection> {
            population: population.clone(),
            next_population: population,
            phantom: PhantomData,
            sel_phantom: PhantomData,
            mutation,
        }
    }

    pub fn run(&mut self, n:u32, iter: i32) {
        self.init(n);

        for i in 0..iter {
            self.step();
            mem::swap(&mut self.population, &mut self.next_population);
            let res = self.is_finished();
            match res {
                None => {}
                Some(result) => {
                    println!("Finished after {} iterations with result: {:?}", i + 1, result);
                    return;
                }
            }
        }
    }

    fn init(&mut self, n: u32) {
        println!("Pop size: {}", self.population.len());
        for i in self.population.iter_mut() {
            for _x in 0..n {
                i.push(rand::thread_rng().gen_range(0, n));
            }
        }
    }

    fn step(&mut self) {
        for i in self.next_population.iter_mut() {
            let a = TSelection::select(&self.population);
            let b = TSelection::select(&self.population);
            let mut c = TCross::cross(&a, &b);
            self.mutation.mutate(&mut c);
            *i = c;
        }
    }

    pub fn is_finished(&self) -> Option<Vec<u32>> {
        for i in self.population.iter() {
            if n_queens::is_valid_solution(&i) {
                return Some(i.clone());
            }
        }
        None
    }
}
